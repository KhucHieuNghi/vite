import Script from 'next/script'
import React from 'react'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {

  return (
    <React.Fragment>
      <Script href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:300,400,500,600" rel="stylesheet" />
      <Script rel='stylesheet' href='https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css' />
      <Script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js'></Script>
      {/* <Script src="/script.js"></Script> */}
      <Component {...pageProps} />
    </React.Fragment>
  )
}

export default MyApp
