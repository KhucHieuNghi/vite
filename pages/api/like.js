import axios from 'axios'

export default async function handler(req, res) {
  const rs = await axios.post('https://8480-115-77-65-84.ap.ngrok.io/api/like', {
      id: req.body.id,
      like: req.body.like
    })

  res.status(200).json(rs.data)
}
