import axios from 'axios';
import React from 'react';

const App = () => {
  const [state, setState] = React.useState()

  const dect = async (e) => {
    e.preventDefault()
    const text = $('#search')[0].value;
    const rs = await axios.post('/api/parse', {
      text
    })
    const { data } = rs;
    console.log("🚀", data)
    setState(data)
  }

  const like = async (like) => {
    axios.post('/api/like', {
      like,
      id: state.id
    })
    setState()
    $('#search')[0].value = '';
  }

  return (
    <main>
      <div className="container">
        <h1>Fancy Search Box</h1>
        <h2>Try below!</h2>
        <div className="search-box">
          <div className="search-icon"><i className="fa fa-search search-icon"></i></div>
          <form action="" className="search-form" onSubmit={dect}>
            <input type="text" placeholder="Search" id="search" autoComplete='off' />
          </form>
          <div className="go-icon"><i className="fa fa-arrow-right"></i></div>
        </div>
        <div className='s-submit'>
          <button className="button-85" role="button" onClick={dect}>Submit</button>
        </div>
        {
          Boolean(state) && (
            <React.Fragment>
              <div style={{ marginTop: '16px', textAlign: 'center' }}>
                {state.result.components.toString()}
              </div>
              <div style={{margin: 'auto', display: 'flex', justifyContent:'center'}}>
              <button className="button-23" role="button" onClick={() => like(0)}>DisLike</button>
              <button className="button-32" role="button" onClick={() => like(1)}>Like</button>
              </div>
            </React.Fragment>
          )
        }

      </div>
    </main>
  )
}

export default App
